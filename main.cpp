/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */





#define ITEMS_PER_PACKET (sizeof(__m128)/sizeof(float))

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <unistd.h>
#include <immintrin.h> // Required to use intrinsic functions

using namespace cimg_library;
double total;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char *SOURCE_IMG = "bailarina.bmp";
const char *DESTINATION_IMG = "bailarina-blanco-negro.bmp";
const int REPETITIONS = 80;

char inicio[] = "./diffImages ";

//Imagen que se quiere comparar,normalemente es la misma que la de salida
char DESTINATION_IMG_COMPARE[] = "bailarina-blanco-negro.bmp ";
//Imagen con la que se quiere comparar.
char DESTINATION_IMG_COMPARE_WITH[] = "bailarina-blanco-negro-single.bmp";

int main() {
	//for (uint i = 0; i < 20; i++){
		// Open file and object initialization
	CImg<data_t> srcImage;
		try
		{
			srcImage.assign(SOURCE_IMG);
		}catch(CImgIOException &exception){
			perror("The source image file does not exist\n");
			exit(-2);
		}

		data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
		data_t *pRdest, *pGdest, *pBdest;
		data_t *pDstImage; // Pointer to the new image pixels
		uint width, height; // Width and height of the image
		uint nComp; // Number of image components


		/***************************************************
		 * TODO: Variables initialization.
		 *   - Prepare variables for the algorithm
		 *   - This is not included in the benchmark time
		 */
		struct timespec tStart, tEnd;
	    double dElapsedTimeS;
		bool excess = false;
		//data_t *pLdest;
        //data_t *pLsrc;
		

		srcImage.display(); // Displays the source image
		width  = srcImage.width(); // Getting information from the source image
		height = srcImage.height();
		nComp  = srcImage.spectrum(); // source image number of components
					// Common values for spectrum (number of image components):
					//  B&W images = 1
					//	Normal color images = 3 (RGB)
					//  Special color images = 4 (RGB and alpha/transparency channel)


        nComp=1; //La imagen es blanco y negro.

		// Allocate memory space for destination image components
		pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
		if (pDstImage == NULL) {
			perror("Allocating destination image");
			exit(-2);
		}

		// Pointers to the componet arrays of the source image
		pRsrc = srcImage.data(); // pRcomp points to the R component array
		pGsrc = pRsrc + height * width; // pGcomp points to the G component array
		pBsrc = pGsrc + height * width; // pBcomp points to B component array

		// Pointers to the RGB arrays of the destination image
		pRdest = pDstImage;
		pGdest = pRdest + height * width;
		pBdest = pGdest + height * width;


		/***********************************************
		 * TODO: Algorithm start.
		 *   - Measure initial time
		 */
		if(clock_gettime(CLOCK_REALTIME, &tStart) == -1)
		{
			perror("ERROR: clock_gettime\n");
			exit(-1);
		}

		for (int j = 0; j < REPETITIONS; j++)
		{
            uint size = height*width;

		/************************************************
		 * FIXME: Algorithm.
		 * In this example, the algorithm is a components swap
		 *
		 * TO BE REPLACED BY YOUR ALGORITHM
		 */

        uint nPackets = (size * sizeof(float)/sizeof(__m128));
    
		int dataInExcess = (size)%(sizeof(__m128)/sizeof(float));

        if ( ((size * sizeof(float))%sizeof(__m128)) != 0) {
            excess = true;
		}
        
		/*
		* Variables empleadas para el SIMD
		*/
        __m128 v1, v2;
        __m128 vrf, vgf, vbf;
		__m128 vr1, vg1, vb1;
        __m128 vr2, vg2, vb2;

		/*
		* Inicialización de los filtros.
		*/
		v2 = _mm_set1_ps(255);
		vrf = _mm_set1_ps(0.3);
		vgf = _mm_set1_ps(0.59);
		vbf = _mm_set1_ps(0.11);
		

		for(uint i = 0; i < nPackets; i++){
			
			/*
			* Se cargan los valores rgb en vectores.
			*/
			vr1 = _mm_loadu_ps((pRsrc + ITEMS_PER_PACKET * i));
			vg1 = _mm_loadu_ps((pGsrc + ITEMS_PER_PACKET * i));
			vb1 = _mm_loadu_ps((pBsrc + ITEMS_PER_PACKET * i));

			
			/*
			* Se multiplican los valores rgb por el filtro correspondiente.
			*/
			vr2 = _mm_mul_ps(vrf, vr1);
			vg2 = _mm_mul_ps(vgf, vg1);
			vb2 = _mm_mul_ps(vbf, vb1);

			/*
			* Se suman los valores RGB.
			*/
			v1 = _mm_add_ps(vr2,vg2);
			v1 = _mm_add_ps(v1,vb2);

			/*
			* Se invierten los valores y se guardan en la posición pRdest correspondiente.
			*/
			*(__m128 *)(pRdest + ITEMS_PER_PACKET * i) = _mm_sub_ps(v2, v1);
		}


		/*
		* Ciclo adicional si existe exceso.
		*/
		if(excess==true){
			nPackets++;
			for (int i =0; i< dataInExcess; i++){
				*(pRdest + nPackets * ITEMS_PER_PACKET + i) = 255 - (*(pRsrc + nPackets * ITEMS_PER_PACKET + i) * 0.3 + 
				*(pGsrc + nPackets * ITEMS_PER_PACKET + i) * 0.59 + 
				*(pBsrc + nPackets * ITEMS_PER_PACKET + i) * 0.11);
			}
		}

		}
		

		/***********************************************
		 * TODO: End of the algorithm.
		 *   - Measure the end time
		 *   - Calculate the elapsed time
		 */
		if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
		{
			perror("ERROR: clock_gettime\n");
			exit(-1);
		}

		dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
		total += dElapsedTimeS;
		printf("Elapsed time    : %f s.\n", dElapsedTimeS);
			
		// Create a new image object with the calculated pixels
		// In case of normal color images use nComp=3,
		// In case of B/W images use nComp=1.
		CImg<data_t> dstImage;
		try
		{
			dstImage.assign(pDstImage, width, height, 1, nComp);
			// Store destination image in disk
			dstImage.save(DESTINATION_IMG); 
		}catch(CImgIOException &exception){
			perror("Unable to create the destination image; MISSING FOLDER\n");
			exit(-2);
		}


		// Display destination image
		dstImage.display();
		
		// Free memory
		free(pDstImage);
	
		printf("Elapsed time    : %f s.\n", total);

		char *start = strcat(inicio,DESTINATION_IMG_COMPARE);
		char *call = strcat(start,DESTINATION_IMG_COMPARE_WITH);
		system(call);

		return 0;
	
}


// int main() {

// 	// Data arrays to sum. May be or not memory aligned to __m256 size (32 bytes)
//     float a[VECTOR_SIZE], b[VECTOR_SIZE];

//     // Calculation of the size of the resulting array
//     // How many 256 bit packets fit in the array?
//     int nPackets = (VECTOR_SIZE * sizeof(float)/sizeof(__m256));

//     // If it is not a exact number we need to add one more packet
//     if ( ((VECTOR_SIZE * sizeof(float))%sizeof(__m256)) != 0) {
//         nPackets++;
// 	}
   
//     // Create an array aligned to 32 bytes (256 bits) memory boundaries to store the sum.
//     // Aligned memory access improves performance    
//     float *c = (float *)_mm_malloc(sizeof(__m256) * nPackets, sizeof(__m256));

//     // 32 bytes (256 bits) packets. Used to stored aligned memory data
//     __m256 va, vb; 

//     // Initialize data arrays
//     for (int i = 0; i < VECTOR_SIZE; i++) {
//         *(a + i) = (float) i;       // a =  0, 1, 2, 3, …
//         *(b + i) = (float) (2 * i); // b =  0, 2, 4, 6, …
//     }

//     // Set the initial c element's value to -1 using vector extensions
//     *(__m256 *) c = _mm256_set1_ps(-1);
//     *(__m256 *)(c + ITEMS_PER_PACKET)     = _mm256_set1_ps(-1);
//     *(__m256 *)(c + ITEMS_PER_PACKET * 2) = _mm256_set1_ps(-1);

//     // Data arrays a and b must not be memory aligned to __m256 data (32 bytes)
//     // so we use intermediate variables to avoid execution errors.
//     // We make an unaligned load of va and vb
//     va = _mm256_loadu_ps(a);      // va = a[0][1]…[7] = 0, 1, 2, 3,  4,  5,  6,  7
//     vb = _mm256_loadu_ps(b);      // vb = b[0][1]…[7] = 0, 2, 4, 6,  8, 10, 12, 14
    
//     // Performs the addition of two aligned vectors, each vector containing 8 floats
//     *(__m256 *)c = _mm256_add_ps(va, vb);// c = c[0][1]…[7] = 0, 3, 6, 9, 12, 15, 18, 21

//     // Next packet
//     // va = a[8][9]…[15] =  8,  9, 10, 11, 12, 13, 14, 15
//     // vb = b[8][9]…[15] = 16, 18, 20, 22, 24, 26, 28, 30
//     //  c = c[8][9]…[15] = 24, 27, 30, 33, 36, 39, 42, 45
//     va = _mm256_loadu_ps((a + ITEMS_PER_PACKET)); 
//     vb = _mm256_loadu_ps((b + ITEMS_PER_PACKET)); 
//     *(__m256 *)(c + ITEMS_PER_PACKET) = _mm256_add_ps(va, vb);

//     // If vectors va and vb have not a number of elements multiple of ITEMS_PER_PACKET 
//     // it is necessary to differentiate the last iteration. 

//     // Calculation of the elements in va and vb in excess
//     int dataInExcess = (VECTOR_SIZE)%(sizeof(__m256)/sizeof(float));

//     // Surplus data can be processed sequentially
    
//     for (int i =0; i< dataInExcess; i++){
//         *(c + 2 * ITEMS_PER_PACKET + i) = *(a + 2 * ITEMS_PER_PACKET + i) + *(b + 2 * ITEMS_PER_PACKET + i);
//     }
    
//     // Print resulting data from array addition
//     for (int i = 0; i < VECTOR_SIZE; i++) {
//         printf("\nc[%d]: %f", i, *(c + i));
// 	}
  
//     // Free memory allocated using _mm_malloc
//     // It has to be freed with _mm_free
//     _mm_free(c);

// 	return 0;
// }
